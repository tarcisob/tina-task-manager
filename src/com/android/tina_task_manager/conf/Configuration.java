package com.android.tina_task_manager.conf;

/**
 * General configuration. Not inicialized
 * @author Anderson
 *
 */
public class Configuration {	
	
	/*
	 * if True, it will notify on the user's Email about the tasks
	 */
	public boolean EMAIL_NOTIFICATION = true;
	
	/*
	 * If true, it will notify the user about late tasks.
	 */
	public boolean LATE_NOTIFICATION = true;
	
	
}
