package com.android.tina_task_manager.logic.managers;

import com.android.tina_task_manager.db.PersistenceImp;

public class NotificationManager {
	
	private static NotificationManager nManager;
	private PersistenceImp DB;
	
	private NotificationManager() {
		DB = DatabaseManager.getInstance();
	}
	
	public NotificationManager getInstance() {
		if (nManager == null) {
			nManager = new NotificationManager();
		}
		
		return nManager;
	}
	
	public void notifyUser() {
		// TODO Auto-generated method stub
	}
	
	public void postponeNotification() {
		// TODO Auto-generated method stub
	}
	
	public void rejectNotification() {
		// TODO Auto-generated method stub
	}
	
	

}
