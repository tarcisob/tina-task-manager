package com.android.tina_task_manager.logic.managers;

import java.util.List;

import com.android.tina_task_manager.db.LocalDB;
import com.android.tina_task_manager.db.PersistenceImp;
import com.android.tina_task_manager.db.PersistenceImplementor;
import com.android.tina_task_manager.logic.model.Task;

public class DatabaseManager implements PersistenceImp {
	
	private static DatabaseManager dbM;
	private PersistenceImplementor DB;
	
	private DatabaseManager() {
		DB = new LocalDB();
		//DB = new SQLiteDB(?);
	}
	
	public static DatabaseManager getInstance() {
		if (dbM == null ) {
			dbM = new DatabaseManager();
		}
		
		return dbM;
	}

	@Override
	public long storeTask(Task t) {
		DB.saveTask(t);
		return t.getId();
	}

	@Override
	public boolean updateTask(Task t) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean removeTask(Task t) {
		DB.deleteTask(t);
		return false;
	}

	@Override
	public Task getTaskById(long id) {
		return DB.getTask(id);
	}

	@Override
	public boolean setAsDone(Task t) {
		t.setDone(true);
		return true;
	}

	@Override
	public boolean setAsNotDone(Task t) {
		t.setDone(false);
		return true;
	}
	
	@Override
	public void open() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Task> getTodaysTasks() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Task> getWeekTasks() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Task> getLateTasks() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Task> getNextTasks() {
		// TODO Auto-generated method stub
		return null;
	}
	

}
