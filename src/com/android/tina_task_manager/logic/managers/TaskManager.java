package com.android.tina_task_manager.logic.managers;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.content.Context;

import com.android.tina_task_manager.db.PersistenceImp;
import com.android.tina_task_manager.db.TasksDataSource;
import com.android.tina_task_manager.logic.model.DailyTask;
import com.android.tina_task_manager.logic.model.Task;

public class TaskManager {

	private static TaskManager tManager;
	private static long nextTaskId; // will use this to generate unique ids for
									// the tasks.
	private PersistenceImp DB;
	public final static String TASK_ID = "TASK_ID";
	public final static String TASKS_IDS = "TASKS_IDS";
	public final static String TASK_NAME = "TASK_NAME";
	public final static String TASK_TIME = "TASK_TIME";

	private TaskManager(Context c) {
		if (c == null) {
			DB = DatabaseManager.getInstance();
		} else {
			DB = TasksDataSource.getInstance(c);
		}
		DB.open();
		// TODO This is a wrong place to make this variable equals 0,
		// every time I use the system the IDs will restart to 0
		nextTaskId = 0;
	}

	public static TaskManager getInstance(Context c) {
		if (tManager == null) {
			tManager = new TaskManager(c);
		}

		return tManager;
	}

	/**
	 * This method creates a new task
	 * 
	 * @param name
	 *            Task's name
	 * @param year
	 *            Task's due year
	 * @param month
	 *            Task's due month
	 * @param day
	 *            Task's due day
	 * @param hour
	 *            Task's due hour
	 * @param minute
	 *            Task's due minute
	 * @param place
	 *            Task's place
	 * @param daily
	 * 			  True if is a daily task
	 * @throws IllegalArgumentException
	 *             If the day, month, year, hour or minute is invalid
	 */
	public int createTask(String name, int year, int month, int day, int hour,
			int minute, String place, boolean daily) throws IllegalArgumentException {

		Calendar dueDate = getCalendar(year, month, day, hour, minute);
		
		if (name == null || name.trim().equals(""))
			throw new IllegalArgumentException("Name cannot be empty");
		if (place == null)
			place = "";

		Task t;
		
		if(daily){
			t = new DailyTask(nextTaskId++, name, dueDate, false, true, place);
		}else{
			t = new Task(nextTaskId++, name, dueDate, false, true, place);
		}
		
		
		int storedId = (int)DB.storeTask(t);
		
		return storedId;
	}
	
	/**
	 * This method gets a specific task by its id
	 * @param id
	 * 		Id of the task the method will return
	 * @return
	 * 		The task with the id given
	 */
	public Task getTask(long id) throws Exception {
		Task task = DB.getTaskById(id);
		if (task == null)
			throw new Exception("Task doesn't exist.");
		
		return task;
	}

	/**
	 * This method edits a specific task by its id
	 * 
	 * @param name
	 *            Updated task's name
	 * @param year
	 *            Updated task's due year
	 * @param month
	 *            Updated task's due month
	 * @param day
	 *            Updated task's due day
	 * @param hour
	 *            Updated task's due hour
	 * @param minute
	 *            Updated task's due minute
	 * @param place
	 *            Updated task's place
	 * @throws IllegalArgumentException
	 *             If the day, month, year, hour or minute is invalid
	 */
	public void updateTask(long taskId, String name, int year, int month,
			int day, int hour, int minute, String place) throws IllegalArgumentException {

		if (name == null || name.trim().equals(""))
			throw new IllegalArgumentException("Name cannot be empty");
		if (place == null)
			place = "";

		Task task = DB.getTaskById(taskId);
		Calendar dueDate = getCalendar(year, month, day, hour, minute);

		task.setName(name);
		task.setDueDate(dueDate);
		task.setPlace(place);
		
		DB.updateTask(task);
	}
	
	/**
	 * This method removes a specific task by its id
	 * @param taskId
	 *            Id of the task to be removed
	 */
	public void removeTask(long taskId) {
		Task task = DB.getTaskById(taskId);
		DB.removeTask(task);
	}

	/**
	 * This method mark a specific task as done or not done
	 * @param taskId
	 *            Id of the task to be marked as done
	 */
	public void setAsDone(long taskId) {
		Task task = DB.getTaskById(taskId);
		DB.setAsDone(task);
		
		if(task instanceof DailyTask){
			((DailyTask) task).setDoneDate( new GregorianCalendar() );
		}
	}
	
	public void resetTaskDaily(long taskId){
		Task task = DB.getTaskById(taskId);
		if ( ((DailyTask) task).getDoneDate().getTime().compareTo( new GregorianCalendar().getTime() ) < 1){
			setAsUndone(taskId);
		}
	}
	
	public void setAsUndone(long taskId) {
		Task task = DB.getTaskById(taskId);
		DB.setAsNotDone(task);
	}
	
	public List<Task> getTodaysTasks() {
		return DB.getTodaysTasks();
	}

	
	private Calendar getCalendar(int year, int month, int day, int hour, int minute) {
		if (year < 1)
			throw new IllegalArgumentException("Year cannot be less than 1");
		if (month < 0 || month > 11)
			throw new IllegalArgumentException(
					"Month cannot be less than 0 or higher than 11");
		if (day < 1 || day > 31)
			throw new IllegalArgumentException(
					"Day cannot be less than 1 or higher than 31");
		if (hour < 0 || hour > 23)
			throw new IllegalArgumentException(
					"Hour cannot be less than 0 or higher than 23");
		if (minute < 0 || minute > 59)
			throw new IllegalArgumentException(
					"Minute cannot be less than 0 or higher than 59");

		Calendar dueDate = new GregorianCalendar(year, month, 1, hour, minute);

		if (day >= dueDate.getActualMaximum(Calendar.DAY_OF_MONTH))
			throw new IllegalArgumentException("Day is not valid"); // If the month ends on 28, 29 or 30

		dueDate.set(Calendar.DAY_OF_MONTH, day);
		
		return dueDate;
	}
	
	public List<Task> getWeekTasks() {
		return DB.getWeekTasks();
	}
	
	public List<Task> getLateTasks() {
		return DB.getLateTasks();
	}
	
	public List<Task> getNextTasks() {
		return DB.getNextTasks();
	}
	
	public long[] getTasksIds(List<Task> tasks) {
		long[] tasksIds = new long[tasks.size()];
		
		for (int i = 0; i < tasks.size(); i++) {
			tasksIds[i] = tasks.get(i).getId();
		}
		
		return tasksIds;
	}
}