package com.android.tina_task_manager.logic.model;

import java.util.Calendar;

public class DailyTask extends Task{
	
	private Calendar doneDate;
	
	public DailyTask(long id, String name, Calendar dueDate, boolean done,
			boolean notifiable, String place) {
		super(id, name, dueDate, done, notifiable, place);
	}

	/**
	 * @return the doneDate
	 */
	public Calendar getDoneDate() {
		return doneDate;
	}

	/**
	 * @param doneDate the doneDate to set
	 */
	public void setDoneDate(Calendar doneDate) {
		this.doneDate = doneDate;
	}

}
