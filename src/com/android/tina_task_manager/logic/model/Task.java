package com.android.tina_task_manager.logic.model;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Task {
	
	private long id;
	private String name;
	private Calendar dueDate;
	private boolean done;
	private boolean notifiable;
	private String place;

	
	public Task(long id, String name, Calendar dueDate, boolean done,
			boolean notifiable, String place) {
		super();
		this.id = id;
		this.name = name;
		this.dueDate = dueDate;
		this.done = done;
		this.notifiable = notifiable;
		this.place = place;
	}
	
	public Task() {
		
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Calendar getDueDate() {
		return dueDate;
	}

	public void setDueDate(Calendar dueDate) {
		this.dueDate = dueDate;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	public boolean isNotifiable() {
		return notifiable;
	}

	public void setNotifiable(boolean notifiable) {
		this.notifiable = notifiable;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}
	
	/**
	 * Method to check if the Task its late
	 * @return
	 */
	public boolean isLate(){
		return (this.getDueDate().compareTo( new GregorianCalendar() ) > 0);
	}
	
	public String getDayOfWeek() {
		switch(dueDate.get(Calendar.DAY_OF_WEEK)) {
			case 1:
				return "Sunday";
			case 2:
				return "Monday";
			case 3:
				return "Tuesday";
			case 4:
				return "Wednesday";
			case 5:
				return "Thursday";
			case 6:
				return "Friday";
			case 7:
				return "Saturday";
			default:
				return "Incompatible day of week field";
		}
	}
	
	public String getTime() {
		StringBuilder timeSB = new StringBuilder();
		timeSB.append(dueDate.get(Calendar.HOUR));
		timeSB.append(":");
		
		int minutes = dueDate.get(Calendar.MINUTE);
		if (minutes < 10) {
			timeSB.append(0);
		} 
		timeSB.append(minutes);
		timeSB.append(dueDate.get(Calendar.AM_PM) == 0? "AM" : "PM");
		
		return timeSB.toString();  
	}
	
}
