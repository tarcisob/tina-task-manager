package com.android.tina_task_manager.db;

import com.android.tina_task_manager.logic.model.Task;

public interface PersistenceImplementor {
	
	public long saveTask(Task t);
	public boolean deleteTask(Task t);
	public Task getTask(long taskId);

}
