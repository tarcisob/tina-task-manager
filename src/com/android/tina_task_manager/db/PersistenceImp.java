package com.android.tina_task_manager.db;

import java.util.List;

import com.android.tina_task_manager.logic.model.Task;

/**
 * @author Tarciso
 * Interface whose methods must be implemented by the classes who will implement
 * the communication with the DataBase.
 */
public interface PersistenceImp {
	public long storeTask(Task t);
	public boolean updateTask(Task t);
	public boolean removeTask(Task t);
	public boolean setAsDone(Task t);
	public boolean setAsNotDone(Task t);
	public void open();
	public Task getTaskById(long id);
	public List<Task> getTodaysTasks();
	public List<Task> getWeekTasks();
	public List<Task> getLateTasks();
	public List<Task> getNextTasks();
}
