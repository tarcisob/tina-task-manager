package com.android.tina_task_manager.db;

import java.util.Calendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.android.tina_task_manager.logic.model.Task;

public class SQLiteDB extends SQLiteOpenHelper implements PersistenceImplementor {
	public static final String TABLE_TASKS = "tasks";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_TASK_NAME = "task";
	public static final String COLUMN_PLACE = "place";
	public static final String COLUMN_DATE = "date";
	public static final String COLUMN_DONE = "done";
	
	private static final String DB_NAME = "tasks.db";
	private static final int DB_VERSION = 1;
	private static final String DB_CREATE = "create table tasks(id integer primary key autoincrement,"
	 		+ " task text not null, place text not null, date bigint, done bit not null);";
	
	private SQLiteDatabase db;

	
	public SQLiteDB(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}
	
	public void open() throws SQLException {
		db = getWritableDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DB_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int vewVersion) {
		db.execSQL("DROP TABLE IF EXISTS tasks");
	    onCreate(db);
	}

	@Override
	public long saveTask(Task t) {
		ContentValues values = new ContentValues();
		values.put(COLUMN_TASK_NAME, t.getName());
		values.put(COLUMN_PLACE, t.getPlace());
		values.put(COLUMN_DATE, t.getDueDate().getTimeInMillis());
		values.put(COLUMN_DONE, t.isDone()? 1 : 0);
		long insertId = db.insert(TABLE_TASKS, null, values);
		return insertId;
	}

	@Override
	public boolean deleteTask(Task t) {
		long id = t.getId();
	    System.out.println("Comment deleted with id: " + id);
	    db.delete(TABLE_TASKS, COLUMN_ID + " = " + id, null);
		return true;
	}

	@Override
	public Task getTask(long taskId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private String getDate(Calendar date) {
		String t = date.get(Calendar.DAY_OF_MONTH) + "-" +
				date.get(Calendar.MONTH) + "-" +
				date.get(Calendar.YEAR);
		Log.e("*************************", t);
		return t;
	}

}