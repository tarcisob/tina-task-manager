package com.android.tina_task_manager.db;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.android.tina_task_manager.logic.model.Task;

public class TasksDataSource implements PersistenceImp {


  private final int SATURDAY = 7; 
	
  // Database fields
  private SQLiteDatabase database;
  private SQLiteDB dbHelper;
  private static TasksDataSource tDS;
  

  private TasksDataSource(Context context) {
    dbHelper = new SQLiteDB(context);
  }
  
  public static TasksDataSource getInstance(Context c) {
	  if (tDS == null) {
		  tDS = new TasksDataSource(c);
	  }
	  return tDS;
  }

  public void open() throws SQLException {
    database = dbHelper.getWritableDatabase();
  }

  public void close() {
    dbHelper.close();
  }

  @Override
  public long storeTask(Task t) {
    ContentValues values = new ContentValues();
    values.put(SQLiteDB.COLUMN_TASK_NAME, t.getName());
    values.put(SQLiteDB.COLUMN_PLACE, t.getPlace());
    values.put(SQLiteDB.COLUMN_DATE, t.getDueDate().getTimeInMillis());
    values.put(SQLiteDB.COLUMN_DONE, 0);
    long insertId = database.insert(SQLiteDB.TABLE_TASKS, null, values);
    
    return insertId;
  }

  @Override
  public boolean updateTask(Task t) {
	long id = t.getId();
	ContentValues values = new ContentValues();
	values.put(SQLiteDB.COLUMN_TASK_NAME, t.getName());
	values.put(SQLiteDB.COLUMN_PLACE, t.getPlace());
	values.put(SQLiteDB.COLUMN_DATE, t.getDueDate().getTimeInMillis());
	values.put(SQLiteDB.COLUMN_DONE, t.isDone()? 1:0);
	database.update(SQLiteDB.TABLE_TASKS, values, SQLiteDB.COLUMN_ID + " = " + id, null);
	
	return true;
  }
  
  public Task getTaskById(long taskId) {
	Task t = null;
	String[] allColumns = {SQLiteDB.COLUMN_ID, SQLiteDB.COLUMN_TASK_NAME, 
			SQLiteDB.COLUMN_PLACE, SQLiteDB.COLUMN_DATE, SQLiteDB.COLUMN_DONE};
	Cursor cursor = database.query(SQLiteDB.TABLE_TASKS,
			allColumns, SQLiteDB.COLUMN_ID + " = " + taskId,
	null, null, null, null);

	cursor.moveToFirst();
	if (!cursor.isAfterLast()) {
	  t = cursorToTask(cursor);
	}
	// make sure to close the cursor
	cursor.close();
	
	return t;
  }

	private Task cursorToTask(Cursor cursor) {
		Task t = new Task();
		t.setId(cursor.getInt(0));
		t.setName(cursor.getString(1));
		t.setPlace(cursor.getString(2));
		Calendar taskTime = Calendar.getInstance();
		taskTime.setTimeInMillis(cursor.getLong(3));
		t.setDueDate(taskTime);
		t.setDone(cursor.getInt(4) == 1? true : false);
		
		return t;
	}
	
	@Override
	public List<Task> getTodaysTasks() {
		List<Task> todaysTasks = new ArrayList<Task>();
		
		Calendar dayStart = Calendar.getInstance();
		dayStart.set(Calendar.HOUR_OF_DAY, 0);
		dayStart.set(Calendar.MINUTE, 0);
		dayStart.set(Calendar.SECOND, 0);
		
		Calendar dayEnd = Calendar.getInstance();
		dayEnd.set(Calendar.HOUR_OF_DAY, 23);
		dayEnd.set(Calendar.MINUTE, 59);
		dayEnd.set(Calendar.SECOND, 59);
		
		long dayStartInMillis = dayStart.getTimeInMillis();
		long dayEndInMillis = dayEnd.getTimeInMillis();
		
		String[] allColumns = {SQLiteDB.COLUMN_ID, SQLiteDB.COLUMN_TASK_NAME, 
				SQLiteDB.COLUMN_PLACE, SQLiteDB.COLUMN_DATE, SQLiteDB.COLUMN_DONE};
		Cursor cursor = database.query(SQLiteDB.TABLE_TASKS,
				allColumns, SQLiteDB.COLUMN_DATE + " between " + dayStartInMillis + " and " + dayEndInMillis,
				null, null, null, SQLiteDB.COLUMN_DATE);
		
		cursor.moveToFirst();
		
		while (!cursor.isAfterLast()) {
		  Task t = cursorToTask(cursor);
		  todaysTasks.add(t);
		  cursor.moveToNext();
		}
		cursor.close();
		return todaysTasks;
	}
	
	@Override
	public boolean removeTask(Task t) {
		database.delete(SQLiteDB.TABLE_TASKS, SQLiteDB.COLUMN_ID + " = " + t.getId(), null);
		return true;
	}
	
	@Override
	public boolean setAsDone(Task t) {
		ContentValues values = new ContentValues();
		values.put(SQLiteDB.COLUMN_DONE, true);
		database.update(SQLiteDB.TABLE_TASKS, values, SQLiteDB.COLUMN_ID + " = " + t.getId(), null);
		return true;
	}
	
	@Override
	public boolean setAsNotDone(Task t) {
		ContentValues values = new ContentValues();
		values.put(SQLiteDB.COLUMN_DONE, false);
		database.update(SQLiteDB.TABLE_TASKS, values, SQLiteDB.COLUMN_ID + " = " + t.getId(), null);
		return true;
	}
	
	@Override
	public List<Task> getWeekTasks() {
		List<Task> weekTasks = new ArrayList<Task>();
		
		Calendar dayStart = Calendar.getInstance();
		dayStart.set(Calendar.HOUR_OF_DAY, 0);
		dayStart.set(Calendar.MINUTE, 0);
		dayStart.set(Calendar.SECOND, 0);
		
		int daysTilWeekEnd = SATURDAY - dayStart.get(Calendar.DAY_OF_WEEK);
		
		Calendar weekEnd = Calendar.getInstance();
		weekEnd.set(Calendar.DAY_OF_MONTH, 
				dayStart.get(Calendar.DAY_OF_MONTH) + daysTilWeekEnd);
		weekEnd.set(Calendar.HOUR_OF_DAY, 23);
		weekEnd.set(Calendar.MINUTE, 59);
		weekEnd.set(Calendar.SECOND, 59);
		
		long dayStartInMillis = dayStart.getTimeInMillis();
		long weekEndInMillis = weekEnd.getTimeInMillis();
		
		String[] allColumns = {SQLiteDB.COLUMN_ID, SQLiteDB.COLUMN_TASK_NAME, 
				SQLiteDB.COLUMN_PLACE, SQLiteDB.COLUMN_DATE, SQLiteDB.COLUMN_DONE};
		Cursor cursor = database.query(SQLiteDB.TABLE_TASKS,
				allColumns, SQLiteDB.COLUMN_DATE + " between " + dayStartInMillis + " and " + weekEndInMillis,
				null, null, null, SQLiteDB.COLUMN_DATE);
		
		cursor.moveToFirst();
		
		while (!cursor.isAfterLast()) {
		  Task t = cursorToTask(cursor);
		  weekTasks.add(t);
		  cursor.moveToNext();
		}
		cursor.close();
		return weekTasks;
	}

	@Override
	public List<Task> getLateTasks() {
		List<Task> lateTasks = new ArrayList<Task>();
		
		Calendar tenMinPast = Calendar.getInstance();
		tenMinPast.set(Calendar.MINUTE, tenMinPast.get(Calendar.MINUTE) - 10);
		
		long tenMinPastInMillis = tenMinPast.getTimeInMillis();
		
		String[] allColumns = {SQLiteDB.COLUMN_ID, SQLiteDB.COLUMN_TASK_NAME, 
				SQLiteDB.COLUMN_PLACE, SQLiteDB.COLUMN_DATE, SQLiteDB.COLUMN_DONE};
		Cursor cursor = database.query(SQLiteDB.TABLE_TASKS,
				allColumns, SQLiteDB.COLUMN_DATE + " < " + tenMinPastInMillis + " and " + SQLiteDB.COLUMN_DONE + " = 0",
				null, null, null, SQLiteDB.COLUMN_DATE);
		
		cursor.moveToFirst();
		
		while (!cursor.isAfterLast()) {
		  Task t = cursorToTask(cursor);
		  lateTasks.add(t);
		  cursor.moveToNext();
		}
		cursor.close();
		return lateTasks;
	}
	
	public List<Task> getNextTasks() {
		List<Task> lateTasks = new ArrayList<Task>();
		
		Calendar now = Calendar.getInstance();
		
		long nowInMillis = now.getTimeInMillis();
		
		String[] allColumns = {SQLiteDB.COLUMN_ID, SQLiteDB.COLUMN_TASK_NAME, 
				SQLiteDB.COLUMN_PLACE, SQLiteDB.COLUMN_DATE, SQLiteDB.COLUMN_DONE};
		Cursor cursor = database.query(SQLiteDB.TABLE_TASKS,
				allColumns, SQLiteDB.COLUMN_DATE + " < " + (nowInMillis + 120000) + " and " + SQLiteDB.COLUMN_DONE + " = 0",
				null, null, null, SQLiteDB.COLUMN_DATE);
		
		cursor.moveToFirst();
		
		while (!cursor.isAfterLast()) {
		  Task t = cursorToTask(cursor);
		  lateTasks.add(t);
		  cursor.moveToNext();
		}
		cursor.close();
		return lateTasks;
	}
} 