package com.android.tina_task_manager.db;

import java.util.ArrayList;
import java.util.List;

import com.android.tina_task_manager.logic.model.Task;

public class LocalDB implements PersistenceImplementor {
	
	private List<Task> tasks;
	
	public LocalDB() {
		super();
		this.tasks = new ArrayList<Task>();
	}

	@Override
	public long saveTask(Task t) {
		int taskIndex = getTaskIndex(t.getId());
		if (taskIndex != -1) {
			tasks.set(taskIndex,t);
		} else {
			tasks.add(t);
		}
		return t.getId();
	}

	@Override
	public boolean deleteTask(Task t) {
		int taskIndex = getTaskIndex(t.getId());
		if (taskIndex != -1) {
			tasks.remove(taskIndex);
		} else {
			return false;
		}
		return true;
	}

	@Override
	public Task getTask(long taskId) {
		for (Task task : tasks) {
			if (task.getId() == taskId) {
				return task;
			}
		}
		return null;
	}
	
	public int getTaskIndex(long taskId) {
		for (int i = 0; i < tasks.size(); i++) {
			if (tasks.get(i).getId() == taskId) {
				return i;
			}
		}
		return -1;
	}

}
