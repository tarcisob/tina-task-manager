package com.android.tina_task_manager.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

public class PushNotificationActivity2 extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);
		
		Toast.makeText(this, "Notification called me!", Toast.LENGTH_LONG).show();
		
	}

}
