package com.android.tina_task_manager.activities;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;


/**
 * REFERENCE: http://marcelosousa.com.br/programacao-android/criando-notificacao-no-android/
 * 
 * TESTANDO!!!
 * 
 * @author Anderson
 *
 */
public class DisplayNotificationActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
 
        //---get the notification ID for the notification; 
        // passed in by the MainActivity---
        int notifID = getIntent().getExtras().getInt("NotifID");
 
        //---PendingIntent to launch activity if the user selects 
        // the notification---
        Intent i = new Intent("AlarmDetails.class");
        i.putExtra("NotifID", notifID);  
 
        PendingIntent detailsIntent = PendingIntent.getActivity(this, 0, i, 0);
 
        NotificationManager nm = (NotificationManager)
            getSystemService(NOTIFICATION_SERVICE);
//ic_launcher � o nome do �cone que aparece na notifica��o ele deve ficar dentro da pasta drawable
       Notification notif = new Notification(R.drawable.ic_launcher, "Notifica��o", System.currentTimeMillis());
       
        /*
        Notification notif = new Notification.Builder( Context.this )
        .setContentTitle("Task Manager") //FIXME colocar melhor titulo para a notifica��o
        .setContentText("notifify") //FIXME colocar um texto melhor
        .setSmallIcon(R.drawable.ic_launcher)
        .build();
        */
        
        CharSequence from = "Minha Notifica��o!";
        CharSequence message = "Descricao da notifica��o";   
        notif.setLatestEventInfo(this, from, message, detailsIntent);
 
        //Som exitido junto com a notifica��o
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {}
 
        //---100ms delay, vibrate for 250ms, pause for 100 ms and
        // then vibrate for 500ms---
        notif.vibrate = new long[] { 100, 250, 100, 500};        
        nm.notify(notifID, notif);
        //Destroi a atividade
        finish();
    }
}