package com.android.tina_task_manager.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import com.android.tina_task_manager.logic.managers.TaskManager;
import com.android.tina_task_manager.logic.model.Task;
import com.android.tina_task_manager.web.managers.EmailManager;

public class MainActivity extends Activity implements OnClickListener,
		OnItemClickListener, OnCheckedChangeListener {

	public enum TaskListingMode {DAILY_TASKS, WEEKLY_TASKS, NOTIFICATION_TASKS};
	public enum NotificationMode {
		NOTIFY_ON_CELLPHONE("Cellphone"), 
		NOTIFY_BY_EMAIL("Email"), 
		NOTIFY_LATE_TASKS("Late");
		
		private String tag;
		private NotificationMode(String tag) {
			this.tag = tag;
		}
		
		public String getTag() {
			return tag;
		}
	};
	
	public final static String TASK_LISTING_MODE = "TASK_LISTING_MODE"; 
	
	private TaskManager taskM;
	TaskListViewAdapter taskListAdapter;
	Switch timeRangeSwitch;
	EmailManager mail;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		taskM = TaskManager.getInstance(this);

//		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_main);
//		if (customTitleSupported) {
//			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
//					R.layout.title_bar);
//		}

		// Fill task list with daily range
		if (getIntent().getSerializableExtra(TASK_LISTING_MODE) == TaskListingMode.NOTIFICATION_TASKS) {
			fillTaskList(TaskListingMode.NOTIFICATION_TASKS);
		} else {
			fillTaskList(TaskListingMode.DAILY_TASKS);
		}

		Button buttonNewTask = (Button) findViewById(R.id.home_new_task_button);
		buttonNewTask.setOnClickListener(this);
		
		timeRangeSwitch = (Switch) findViewById(R.id.time_range_switch);
		timeRangeSwitch.setOnCheckedChangeListener(this);
	}

//	private void pushNotifications(final boolean notifyOnCellphone, 
//			final boolean notifyByEmail, final boolean notifyLateTasks) {
		
//		final Handler handler = new Handler();
//		handler.postDelayed(new Runnable() {
//		    public void run() {
//		    	
//		    	if (notifyLateTasks) {
//		    		for (Task t : taskM.getLateTasks()) {
//		    			showNotification("Late Task: " + t.getName());	
//		    		}
//		    		handler.postDelayed(this, 5000); //now is every 2 minutes
//		    	} else if (notifyOnCellphone) {
//		    		for (Task t : taskM.getNextTasks()) {
//		    			showNotification("Task reminder: " + t.getName());	
//		    		}
//		    		handler.postDelayed(this, 120000); //now is every 2 minutes
//		    	}
//		    }
//		 }, 120000);
//	}
	
	private void fillTaskList(TaskListingMode listingMode) {

		List<Task> taskList = new ArrayList<Task>();

		if (listingMode == TaskListingMode.WEEKLY_TASKS) {
			taskList = taskM.getLateTasks();
		} else if (listingMode == TaskListingMode.DAILY_TASKS) {
			taskList = taskM.getTodaysTasks();
		} else if (listingMode == TaskListingMode.NOTIFICATION_TASKS) {
			Task t = null;
			try {
				t = taskM.getTask(getIntent().getLongExtra(TaskManager.TASK_ID, 0));
			} catch (Exception e) {
				Toast.makeText(this, "Could not retrieve task info: " + 
						e.getMessage(), Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
			if (t != null) {
				taskList.add(t);
			}
		}

		List<Map<String, String>> taskDetails = new ArrayList<Map<String, String>>();

		for (Task t : taskList) {
			Map<String, String> taskMap = new HashMap<String, String>();
			taskMap.put(TaskManager.TASK_ID, String.valueOf(t.getId()));
			taskMap.put(TaskManager.TASK_NAME, t.getName());
			if (listingMode == TaskListingMode.DAILY_TASKS) {
				taskMap.put(TaskManager.TASK_TIME, t.getTime());
			} else {
				taskMap.put(TaskManager.TASK_TIME,
						t.getDayOfWeek() + " - " + t.getTime());
			}
			taskDetails.add(taskMap);
		}

		taskListAdapter = new TaskListViewAdapter(this, taskDetails);

		ListView listView = (ListView) findViewById(R.id.home_task_list);
		listView.setAdapter(taskListAdapter);
		listView.setOnItemClickListener(this);
	}
	
	public void showNotification(String message) {
		Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

		Intent intent = new Intent(MainActivity.this, CellphoneNotificationReceiver.class);
		PendingIntent pIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, 0);

		Notification myNotification = new Notification.Builder(this)
			.setContentTitle("Remember.me")
			.setContentText(message)
			.setSmallIcon(R.drawable.notification_icon)
			.setContentIntent(pIntent)
			.setSound(soundUri)
			.addAction(R.drawable.notification_icon, "View", pIntent)
			.addAction(0, "Remind", pIntent)
			.build();

		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		myNotification.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(0, myNotification);
	}


	@Override
	public void onClick(View arg0) {
		if (arg0.getId() == R.id.home_new_task_button) {
			Intent intentNewTask = new Intent(MainActivity.this, NewTask.class);
			MainActivity.this.startActivity(intentNewTask);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
	    return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		long selectedTaskId = arg0.getItemIdAtPosition(arg2);
		Intent intentNewTask = new Intent(MainActivity.this, EditTaskActivity.class);
		intentNewTask.putExtra(TaskManager.TASK_ID, selectedTaskId);
		MainActivity.this.startActivity(intentNewTask);
	}

	@Override
	public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
		fillTaskList(arg1? TaskListingMode.WEEKLY_TASKS : TaskListingMode.DAILY_TASKS);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			Intent intentSettingsActivity = new Intent(this, SettingsActivity.class);
			startActivity(intentSettingsActivity);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}