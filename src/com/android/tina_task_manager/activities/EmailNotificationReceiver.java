package com.android.tina_task_manager.activities;

import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.android.tina_task_manager.logic.managers.TaskManager;
import com.android.tina_task_manager.logic.model.Task;
import com.android.tina_task_manager.web.notifications.EmailExpert;

public class EmailNotificationReceiver extends BroadcastReceiver {
	private TaskManager taskManager;
	private EmailExpert mail = new EmailExpert();
	
	Context context;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		this.context = context;
		
		
		taskManager = TaskManager.getInstance(context);
		sendMail(PreferenceManager.getDefaultSharedPreferences(context).getString("textedit_email", "").trim());
	}
	
	public void sendMail(String to){
		
		List<Task> todaysTasks = taskManager.getTodaysTasks();
			
			StringBuilder message = new StringBuilder();

			message.append("Hello, \n\n" + "we are reminding you about your tasks, check today's tasks."
							+ " If one or more tasks was already done, "
							+ "please update in your android remeber.me to avoid us show some remiding"
							+ " Emails about task that you already did. \n\n");

			if (todaysTasks.size() != 0) {
			for (Task t : todaysTasks) {
				message.append(protocolTodayMessage(t));
				message.append("\n");
			}
			}

			try {
				mail.sendMail("tina.taskmanager@gmail.com", to, "remember.me - no reply", message.toString());
				Toast.makeText(context, "To: "+ to + "Message: " + message.toString(), Toast.LENGTH_LONG).show();
			} catch (Throwable t) {
				Toast.makeText(context, "Could not send email to "+ to + ": " + t.getMessage(), Toast.LENGTH_LONG).show();
			}
	}

	private String protocolTodayMessage(Task t) {
		StringBuilder message = new StringBuilder();
		message.append(t.getName());
		message.append(", on due date: ");
		message.append(t.getTime());
		
		return message.toString();
	}
}
