package com.android.tina_task_manager.activities;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.widget.Toast;

public class SettingsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {
	/*
	To use the preferences:
	SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
	boolean emailIsEnabled = sharedPrefs.getBoolean("checkbox_notifications_by_email", false);
	String emailAddress = sharedPrefs.getString("textedit_email", "");
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			onCreatePreferenceActivity();
		} else {
			onCreatePreferenceFragment();
		}
		
		PreferenceManager.getDefaultSharedPreferences(this)
    	.registerOnSharedPreferenceChangeListener(this);
	}
	
	@Override
	protected void onResume() {
	    super.onResume();
	    PreferenceManager.getDefaultSharedPreferences(this)
	    	.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
	    super.onPause();
	    PreferenceManager.getDefaultSharedPreferences(this)
	            .unregisterOnSharedPreferenceChangeListener(this);
	}

	@SuppressWarnings("deprecation")
	private void onCreatePreferenceActivity() {
		addPreferencesFromResource(R.xml.settings);
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		sharedPrefs.getBoolean("checkbox_notifications_by_email", false);
		sharedPrefs.getString("textedit_email", "");
	}

	@SuppressLint("NewApi")
	private void onCreatePreferenceFragment() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
		 
	}

	public static class MyPreferenceFragment extends PreferenceFragment {
		@Override
		public void onCreate(final Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.settings);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		
		if (key.equals("checkbox_notifications_device")) {
			boolean notificationsIsEnabled = sharedPreferences.getBoolean("checkbox_notifications_device", false);
			setNextNotificationAlarm(notificationsIsEnabled);
			
		} else if (key.equals("checkbox_notifications_by_email") || key.equals("textedit_email")) {
			boolean emailIsEnabled = sharedPreferences.getBoolean("checkbox_notifications_by_email", false);
			String emailAddress = sharedPreferences.getString("textedit_email", "");
			
			if (emailAddress.trim().equals(""))
				emailIsEnabled = false;
			setEmailNotificationAlarm(emailIsEnabled, emailAddress);
		}
	}
	
	private void setEmailNotificationAlarm(boolean enableNotification, String emailAddress) {
		final long EMAIL_NOTIFICATION_INTERVAL = 1000 * 10;//60 * 60 * 24;
		
		Intent alarmIntent = new Intent(this, EmailNotificationReceiver.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
		AlarmManager aManager = (AlarmManager) getSystemService(ALARM_SERVICE);

		if (enableNotification) {
			aManager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis()
							+ EMAIL_NOTIFICATION_INTERVAL, EMAIL_NOTIFICATION_INTERVAL, pendingIntent);

			Toast.makeText(this, "Just set email notifications!", Toast.LENGTH_LONG).show();
		} else {
			aManager.cancel(pendingIntent);
			
			Toast.makeText(this, "Just unset email notifications!", Toast.LENGTH_LONG).show();
		}
	}
	
	private void setNextNotificationAlarm(boolean enableNotification) {
		final long NOTIFICATION_INTERVAL = 1000 * 15;
		
		Intent alarmIntent = new Intent(this, CellphoneNotificationReceiver.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(
				this, 0, alarmIntent, 0);
		
		AlarmManager aManager = (AlarmManager) getSystemService(ALARM_SERVICE);

		if (enableNotification) {
			aManager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis()
							+ NOTIFICATION_INTERVAL, NOTIFICATION_INTERVAL, pendingIntent);

			Toast.makeText(this, "Just set notification service!", Toast.LENGTH_LONG).show();
		} else {
			aManager.cancel(pendingIntent);

			Toast.makeText(this, "Just set notification service!", Toast.LENGTH_LONG).show();
		}
	}
}
