package com.android.tina_task_manager.activities;

import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tina_task_manager.logic.managers.TaskManager;

public class TaskListViewAdapter extends BaseAdapter {

	private Activity activity;
	private List<Map<String, String>> data;
	private static LayoutInflater inflater = null;

	public TaskListViewAdapter(Activity a, List<Map<String, String>> d) {
		activity = a;
		data = d;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return Long.valueOf(data.get(position).get(TaskManager.TASK_ID));
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.task_list_element, null);

		TextView taskName = (TextView) vi.findViewById(R.id.home_task_name);
		TextView taskTime = (TextView) vi.findViewById(R.id.home_task_time);

		Map<String, String> task = data.get(position);

		// Setting all values in listview
		taskName.setText(task.get(TaskManager.TASK_NAME));
		taskTime.setText(task.get(TaskManager.TASK_TIME));
		long taskId = Long.valueOf(task.get(TaskManager.TASK_ID));

		Button checkTaskDoneBtn = (Button) vi
				.findViewById(R.id.home_check_task_as_done_btn);
		checkTaskDoneBtn.setTag(position);
		setCheckAsDoneClickListner(checkTaskDoneBtn, taskId);

		Button removeTaskBtn = (Button) vi
				.findViewById(R.id.home_remove_task_btn);
		removeTaskBtn.setTag(position);
		setRemoveClickListner(removeTaskBtn, taskId);

		try {
			if (TaskManager.getInstance(activity).getTask(taskId).isDone()) {
				checkTaskDoneBtn.setBackground(activity.getResources().getDrawable(R.drawable.uncheck_task_done_icon));
			} 
		} catch (Exception e) {
			Toast.makeText(activity, "Error: Task is done? ID: " + taskId,
					Toast.LENGTH_SHORT).show();
		}

		return vi;
	}

	private void setCheckAsDoneClickListner(Button checkTaskDoneBtn,
			final long taskId) {

		checkTaskDoneBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
				
				try {
					final boolean isTaskDone = TaskManager.getInstance(activity).getTask(taskId).isDone();
					
					AlertDialog.Builder alertDialog = new AlertDialog.Builder(
							activity);
					alertDialog.setTitle("Change Task Status");
					
					if (isTaskDone) {
						alertDialog.setMessage("Are you sure you want to check this task as undone?");
					} else {
						alertDialog.setMessage("Are you sure you want to check this task as done?");
					}
					alertDialog.setPositiveButton("YES",
							new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int which) {
							if (isTaskDone) {
								TaskManager.getInstance(activity).setAsUndone(taskId);
								v.setBackground(activity.getResources().getDrawable(R.drawable.check_task_done_icon));
								Toast.makeText(activity,
										"Task checked as undone",
										Toast.LENGTH_SHORT).show();
							} else {
								TaskManager.getInstance(activity).setAsDone(taskId);
								v.setBackground(activity.getResources().getDrawable(R.drawable.uncheck_task_done_icon));
								Toast.makeText(activity,
										"Task checked as done",
										Toast.LENGTH_SHORT).show();
							}
						}
					});
					alertDialog.setNegativeButton("NO",
							new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int which) {
							dialog.cancel();
						}
					});
					alertDialog.show();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	private void setRemoveClickListner(Button removeTaskBtn, final long taskId) {

		removeTaskBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(
						activity);
				alertDialog.setTitle("Remove task");
				alertDialog
						.setMessage("Are you sure you want to delete this task? (cannot be reverted)");
				alertDialog.setPositiveButton("YES",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								TaskManager.getInstance(activity).removeTask(
										taskId);
								activity.recreate();
								Toast.makeText(activity,
										"Task successfully removed",
										Toast.LENGTH_SHORT).show();
							}
						});
				alertDialog.setNegativeButton("NO",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.cancel();
							}
						});
				alertDialog.show();

			}
		});
	}

}
