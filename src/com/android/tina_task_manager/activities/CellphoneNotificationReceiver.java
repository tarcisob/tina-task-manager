package com.android.tina_task_manager.activities;

import java.util.List;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.widget.Toast;

import com.android.tina_task_manager.logic.managers.TaskManager;
import com.android.tina_task_manager.logic.model.Task;

public class CellphoneNotificationReceiver extends BroadcastReceiver {

	TaskManager tManager;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Toast.makeText(context, "Called Late Notification Receiver!", Toast.LENGTH_LONG).show();
		
		tManager = TaskManager.getInstance(context);
		
		List<Task> upcomingTasks = tManager.getNextTasks();
		if (upcomingTasks.size() != 0) {
			for (Task t : upcomingTasks) {
				showNotification(context, "You have an Upcoming Task: " + t.getName(), t.getId());
			}
		}
		
		
		List<Task> lateTasks = tManager.getLateTasks();
		if (lateTasks.size() != 0) {
			for (Task t : lateTasks) {
				showNotification(context, "You have a Late Task: " + t.getName(), t.getId());
			}
		}
	}
	
	private void showNotification(Context c, String message, long taskId) {
		
		Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		Intent intent = new Intent(c, MainActivity.class);
		intent.putExtra(MainActivity.TASK_LISTING_MODE, MainActivity.TaskListingMode.NOTIFICATION_TASKS);
		intent.putExtra(TaskManager.TASK_ID, taskId);
		PendingIntent pIntent = PendingIntent.getActivity(c, 0, intent, 0);

		Notification myNotification = new Notification.Builder(c)
			.setContentTitle("Remember.me")
			.setContentText(message)
			.setSmallIcon(R.drawable.notification_icon)
			.setContentIntent(pIntent)
			.setSound(soundUri)
			.addAction(R.drawable.notification_icon, "View", pIntent)
			.addAction(0, "Remind", pIntent)
			.build();

		NotificationManager notificationManager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
		myNotification.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(0, myNotification);
	}
}
