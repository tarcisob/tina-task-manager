package com.android.tina_task_manager.activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.tina_task_manager.logic.managers.TaskManager;

public class NewTask extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_task);
		// Show the Up button in the action bar.
//		setupActionBar();
		setButtonSaveAction((Button) findViewById(R.id.newTaskButtonDone));
	}

	private void setButtonSaveAction(Button button) {
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				EditText textFieldName = (EditText) findViewById(R.id.newTaskName);
				String taskName = textFieldName.getText().toString();

				EditText textFieldPlace = (EditText) findViewById(R.id.newTaskPlace);
				String taskPlace = textFieldPlace.getText().toString();

				DatePicker taskDate = (DatePicker) findViewById(R.id.newTaskDatePicker);
				int taskYear = taskDate.getYear();
				int taskMonth = taskDate.getMonth();
				int taskDay = taskDate.getDayOfMonth();

				TimePicker taskTime = (TimePicker) findViewById(R.id.newTaskTimePicker);
				int taskHour = taskTime.getCurrentHour();
				int taskMinute = taskTime.getCurrentMinute();
				
//				CheckBox checkbox = (CheckBox) findViewById(R.id.checkBoxDaily);
//				boolean daily = checkbox.isActivated();

				createNewTask(taskName, taskPlace, taskYear, taskMonth,
						taskDay, taskHour, taskMinute, false);
			}
		});

	}

	private void createNewTask(String name, String place, int year, int month,
			int day, int hour, int minute, boolean daily) {
		try {
			int newTaskId = TaskManager.getInstance(this).createTask(name, year, month, day, hour,
					minute, place, daily);

//			Toast toast = Toast.makeText(NewTask.this, "New Task Created with id: " + newTaskId,
//					Toast.LENGTH_LONG);
//			toast.show();

			NavUtils.navigateUpFromSameTask(NewTask.this);

		} catch (Throwable t) {
			Toast toast = Toast.makeText(NewTask.this, t.getMessage(),
					Toast.LENGTH_SHORT);
			toast.show();
		}

	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
