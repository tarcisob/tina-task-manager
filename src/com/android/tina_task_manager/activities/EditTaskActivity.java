package com.android.tina_task_manager.activities;

import java.util.Calendar;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.tina_task_manager.logic.managers.TaskManager;
import com.android.tina_task_manager.logic.model.Task;

public class EditTaskActivity extends Activity {

	private TaskManager manager;
	private long taskBeingEdited;

	private EditText textFieldName;
	private EditText textFieldPlace;
	private DatePicker taskDate;
	private TimePicker taskTime;
	private CheckBox checkbox;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		taskBeingEdited = getIntent().getLongExtra(TaskManager.TASK_ID, 0);

		setContentView(R.layout.activity_new_task);
		// Show the Up button in the action bar.
		// setupActionBar();
		manager = TaskManager.getInstance(this);

		textFieldName = (EditText) findViewById(R.id.newTaskName);
		textFieldPlace = (EditText) findViewById(R.id.newTaskPlace);
		taskDate = (DatePicker) findViewById(R.id.newTaskDatePicker);
		taskTime = (TimePicker) findViewById(R.id.newTaskTimePicker);
//		checkbox = (CheckBox) findViewById(R.id.checkBoxDaily);

		try {
			Task task = manager.getTask(taskBeingEdited);

			textFieldName.setText(task.getName());
			textFieldPlace.setText(task.getPlace());
			Calendar dueDate = task.getDueDate();
			taskDate.updateDate(dueDate.get(Calendar.YEAR),
					dueDate.get(Calendar.MONTH),
					dueDate.get(Calendar.DAY_OF_MONTH));
			taskTime.setCurrentHour(dueDate.get(Calendar.HOUR_OF_DAY));
			taskTime.setCurrentMinute(dueDate.get(Calendar.MINUTE));
		} catch (Exception e) {
			Toast.makeText(EditTaskActivity.this, e.getMessage(),
					Toast.LENGTH_SHORT).show();
		}

		setButtonSaveAction((Button) findViewById(R.id.newTaskButtonDone));
	}

	private void setButtonSaveAction(Button button) {
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String taskName = textFieldName.getText().toString();

				String taskPlace = textFieldPlace.getText().toString();

				int taskYear = taskDate.getYear();
				int taskMonth = taskDate.getMonth();
				int taskDay = taskDate.getDayOfMonth();

				int taskHour = taskTime.getCurrentHour();
				int taskMinute = taskTime.getCurrentMinute();

//				boolean daily = checkbox.isActivated();

				editTask(taskName, taskPlace, taskYear, taskMonth, taskDay,
						taskHour, taskMinute, false);
			}
		});

	}

	private void editTask(String name, String place, int year, int month,
			int day, int hour, int minute, boolean daily) {
		try {
			manager.updateTask(taskBeingEdited, name, year, month, day, hour,
					minute, place);

			Toast toast = Toast.makeText(EditTaskActivity.this, "Task Updated",
					Toast.LENGTH_LONG);
			toast.show();

			NavUtils.navigateUpFromSameTask(EditTaskActivity.this);

		} catch (Throwable t) {
			Toast toast = Toast.makeText(EditTaskActivity.this, t.getMessage(),
					Toast.LENGTH_SHORT);
			toast.show();
		}

	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.new_task, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
