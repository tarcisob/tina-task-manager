package com.android.tina_task_manager.web.managers;

import com.android.tina_task_manager.logic.model.Task;
import com.android.tina_task_manager.web.notifications.EmailExpert;

/**
 * 
 * @author anderson
 *
 */

public class EmailManager {
	private EmailExpert mail = new EmailExpert();
	
	public void sendMail(String to){
		
		StringBuilder message = new StringBuilder();
		
		message.append("Hello, \n\n" +
				 "we are reminding you about your tasks, check today's tasks and late tasks."
				 + " If one or more tasks was already done, " + 
				 "please update in your Android Task Manager to avoid us show some remiding"
				 + " Emails about task that you already did. \n\n");
		
		/*for (Task t : pers.getLateTasks()){
			message.append( protocolLateMessage (t));		
		}
		for (Task t : pers.getTodaysTasks()){
			message.append( protocolTodayMessage (t));		
		}*/
		
		
		//FIXME testar Email tinaTaskManager
		mail.sendMail("tina.taskmanager@gmail.com", to, "remember.me - no reply", message.toString());
		
	}

	private String protocolTodayMessage(Task t) {
		StringBuilder message = new StringBuilder();

		message.append(t.getName());
		message.append(", on due date: ");
		message.append(t.getTime());
		message.append(" it's today!");		
	
	return message.toString();
	}

	//TODO terminar esse metodo para todas as configurações
	private String protocolLateMessage(Task t) {
		
		StringBuilder message = new StringBuilder();
		

			message.append(t.getName());
			message.append(", on due date: ");
			message.append(t.getTime());
			message.append(" it's late!\n");		
		
		return message.toString();
	}	

}
