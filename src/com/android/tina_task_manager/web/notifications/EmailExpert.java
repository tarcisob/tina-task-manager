package com.android.tina_task_manager.web.notifications;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.util.Properties;

/**
 * Expert for Send Emails
 * @author Anderson
 *
 */
public class EmailExpert {
	   
	  private String mailSMTPServer;
	  private String mailSMTPServerPort;
	  private String mailSenha;
	  
	  /**
	   * Send new Email
	   * @param from
	   * 			Who its send the mail
	   * @param to
	   * 			Who will get the mail
	   * @param subject
	   * 			Title or subject of the mail
	   * @param message
	   * 			Mail Message
	   */
	  public void sendMail(final String from, String to, String subject, String message){
		  
		  Properties properties = new Properties();
		  
		  mailSMTPServer = "smtp.googlemail.com";
		  mailSMTPServerPort = "456";
		  
		  //FIXME senha do novo email
		  mailSenha = "t1n4t45km4n4g3r";
		  
		  
		  properties.put("mail.transport.protocol", "smtp"); //Define o protocolo de envio para SMTP
		  properties.put("mail.smtp.starttls.enable", "true");
		  properties.put("mail.smtp.host", mailSMTPServer); //Server SMTP do gmail
		  properties.put("mail.smtp.auth", true); //Ativa autentifica��o
		  properties.put("mail.smtp.user", from); //Usuario, ou seja, a conta que est� enviando o email (tem que ser do Gmail)
		  properties.put("mail.debug", true);
		  properties.put("mail.smtp.port", mailSMTPServerPort); //porta
		  properties.put("mail.smtp.socketFactory.port", mailSMTPServerPort);
		  properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		  properties.put("mail.smtp.socketFactory.fallback", false);
		  
		  
		  Session session = Session.getDefaultInstance(properties,
                  new javax.mail.Authenticator() {
                       protected PasswordAuthentication getPasswordAuthentication() 
                       {
                             return new PasswordAuthentication( from , mailSenha);
                       }
                  });
		  

		  session.setDebug(true);
		  
		  try {

              Message msg = new MimeMessage(session);
              msg.setFrom(new InternetAddress( from )); //Remetente

              Address[] toUser = InternetAddress //Destinat�rio(s)
                         .parse(to); //pode colar mais de um email 

              msg.setRecipients(Message.RecipientType.TO, toUser);
              msg.setSubject(subject);//Assunto
              msg.setText(message);
              /**M�todo para enviar a mensagem criada*/
              Transport.send(msg);

         } catch (MessagingException e) {
              throw new RuntimeException(e);
        }
  }
}
