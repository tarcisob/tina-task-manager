package com.android.tina_task_manager.logic.managers;

import static org.junit.Assert.*;
import java.util.Calendar;
import org.junit.Before;
import org.junit.Test;

public class TestEditTask {
	
	private TaskManager tm;
	private int taskId;
	
	@Before
	public void testInit() {
		tm = TaskManager.getInstance(null);
		taskId = tm.createTask("Testando", 2013, 11, 3, 12, 10, "Place", false);
	}

	@Test
	public void editTaskName() throws Exception {
		assertEquals("Task name", tm.getTask(taskId).getName(), "Testando");
		
		tm.updateTask(taskId, "New Task Name", 2013, 11, 3, 12, 10, "Place");
		assertEquals("Task name", tm.getTask(taskId).getName(), "New Task Name");
	}
	
	@Test
	public void editTaskDate() throws Exception {
		assertEquals("Task due day", tm.getTask(taskId).getDueDate().get(Calendar.DAY_OF_MONTH), 3);
		assertEquals("Task due day", tm.getTask(taskId).getDueDate().get(Calendar.MONTH), 11);
		assertEquals("Task due day", tm.getTask(taskId).getDueDate().get(Calendar.YEAR), 2013);
				
		tm.updateTask(taskId, "Task Name", 2014, 0, 3, 12, 10, "Place");

		assertEquals("Task due day", tm.getTask(taskId).getDueDate().get(Calendar.DAY_OF_MONTH), 3);
		assertEquals("Task due day", tm.getTask(taskId).getDueDate().get(Calendar.MONTH), 0);
		assertEquals("Task due day", tm.getTask(taskId).getDueDate().get(Calendar.YEAR), 2014);
	}
	
	@Test
	public void editTaskPlace() throws Exception {
		assertEquals("Task place", tm.getTask(taskId).getPlace(), "Place");
		
		tm.updateTask(taskId, "Task Name", 2013, 11, 3, 0, 0, "New Place");
		assertEquals("Task place", tm.getTask(taskId).getPlace(), "New Place");
	}	
}