package com.android.tina_task_manager.logic.managers;

import org.junit.Before;
import org.junit.Test;

public class TestCreateTask {
	
	TaskManager tm;
	
	@Before
	public void testInit() {
		tm = TaskManager.getInstance(null);
	}

	@Test
	public void createTaskYearMontDay_One() {
		tm.createTask("name", 1, 1, 1, 0, 0, "place", false);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void createTaskYearNegative() {
		tm.createTask("name", -1, 1, 1, 0, 0, "place", false);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void createTaskYearZero() {
		tm.createTask("name", 0, 1, 1, 0, 0, "place", false);
	}
	
	@Test
	public void createTaskYearBig() {
		tm.createTask("name", 1000000, 1, 1, 0, 0, "place", false);
	}

	@Test(expected=IllegalArgumentException.class)
	public void createTaskMonthNegative() {
		tm.createTask("name", 1, -1, 1, 0, 0, "place", false);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void createTaskMonthLessThanZero() {
		tm.createTask("name", 1, -1, 1, 0, 0, "place", false);
	}
	
	@Test
	public void createTaskMonthMax() {
		tm.createTask("name", 1, 11, 1, 0, 0, "place", false);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void createTaskMonthOverflow() {
		tm.createTask("name", 1, 13, 1, 0, 0, "place", false);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void createTaskDayNegative() {
		tm.createTask("name", 1, 1, -1, 0, 0, "place", false);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void createTaskDayZero() {
		tm.createTask("name", 1, 1, 0, 0, 0, "place", false);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void createTaskDay32October() {
		tm.createTask("name", 1, 10, 32, 0, 0, "place", false);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void createTaskDay31November() {
		tm.createTask("name", 2013, 11, 31, 0, 0, "place", false);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void createTaskDay29Frebruary() {
		tm.createTask("name", 2014, 1, 29, 0, 0, "place", false);
	}
	
	@Test
	public void createTaskDay29FrebruaryBisext() {
		tm.createTask("name", 2012, 2, 29, 0, 0, "place", false);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void createTaskNamePlace() {
		tm.createTask(null, 1, 1, 1, 0, 0, null, false);
		tm.createTask("", 1, 1, 1, 0, 0, "", false);
	}
}