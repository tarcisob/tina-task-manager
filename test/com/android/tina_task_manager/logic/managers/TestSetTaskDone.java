package com.android.tina_task_manager.logic.managers;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TestSetTaskDone {
	
	TaskManager tm;
	int taskId;
	
	@Before
	public void testInit() {
		tm = TaskManager.getInstance(null);
		taskId = tm.createTask("Task Name", 2013, 11, 3, 0, 0, "Place", false);
	}
	
	@Test
	public void testSetTaskDone() throws Exception {
		assertFalse("Task starts as not done", tm.getTask(taskId).isDone());
		
		tm.setAsDone(taskId);
		assertTrue("Task is done", tm.getTask(taskId).isDone());
		
		tm.setAsUndone(taskId);
		assertFalse("Task is not done anymore", tm.getTask(taskId).isDone());
	}

}