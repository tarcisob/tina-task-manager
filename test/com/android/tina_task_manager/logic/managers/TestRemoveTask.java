package com.android.tina_task_manager.logic.managers;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TestRemoveTask {

	TaskManager tm;
	int taskId;
	
	@Before
	public void testInit() {
		tm = TaskManager.getInstance(null);
		taskId = tm.createTask("Task Name", 2013, 11, 3, 0, 0, "Place", false);
	}
	
	@Test
	public void testTaskExists() throws Exception {
		assertEquals("Task is not done yet", tm.getTask(taskId).getId(), taskId);
	}
	
	@Test
	public void testRemoveTask() {
		tm.removeTask(taskId);
		try {
		    tm.getTask(taskId);
		    fail("Exception not throw!");
		} catch(Exception ex) {
		    assertTrue("Task doen's exist", ex.getMessage().equals("Task doesn't exist."));
		}
	}
	
}